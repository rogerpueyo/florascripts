# FLoRaScripts

A bunch of scripts to process .sca (scalar) files from FLoRaMesh simulations
with OMNeT++.

## **PLTR**: FLoRaMesh plotter

This script uses Gnuplot to generate graphs from a comma-separated-values (CSV)
file.

## **PLTREP**: FLoRaMesh plotter with repetitions

This script uses Gnuplot to generate graphs from a comma-separated-values (CSV)
file with the sttdev from the simulation repetitions.

## **SCAD**: .sca to dir

This script creates required directories, on a per-FLoRaMesh OMNeT++ .ini
[Config] basis (e.g., `LM7_11_001_grid_SF7_SF12`), and classifies .sca files into
them. Older files are only overwritten by newer ones equal size or bigger.

## **SCAPREP**: .sca processing with repetitions

This script processes FLoRaMesh OMNeT++ .sca simulation result files with
repetitions in the directory, and exports the values for a number of parameters
and results to a comma-separated values (CSV) file.

## **SLMP**: Slurm prune

This script prunes Slurm's task output files (e.g., `slurm-4792751.out`) from successful simulations.

---

## Legacy scripts

### **SCAP**: .sca processing

This script processes FLoRaMesh OMNeT++ .sca simulation result files, and
exports the values for a number of parameters and results to a comma-separated
values (CSV) file. It does not handle simulation repetitions.

Superseded by SCAPREP.

## **SCAPREP_legacy**: .sca processing with repetitions

This script processes FLoRaMesh OMNeT++ .sca simulation result files with
repetitions in the directory, and exports the average and stddev values for a
number of parameters and results to a comma-separated values (CSV) file.

Superseded by current SCAPREP.
